package TermProject.Fall2022.Mescher;

public class ByzYearDateDecorator implements DateOutputDecorator {
  private DateOutput date;

  public ByzYearDateDecorator(DateOutput date) {
    this.date = date;
  }

  public void setDay(int day) {
    date.setDay(day);
  }

  public void setMonth(int month) {
    date.setMonth(month);
  }

  public void setYear(int year) {
    if (date.getMonth() > 8) {
      date.setYear(year - 5509);
    } else {
      date.setYear(year - 5508);
    }
  }

  public int getDay() {
    return date.getDay();
  }

  public int getMonth() {
    return date.getMonth();
  }

  public int getYear() {
    if (date.getMonth() > 8) {
      return date.getYear() + 5509;
    } else {
      return date.getYear() + 5508;
    }
  }

  public String getDateString() {
    return String.format("%d/%d/%d", getYear(), getMonth(), getDay());
  }

  public void shift(int days) {
    date.shift(days);
  }

}
