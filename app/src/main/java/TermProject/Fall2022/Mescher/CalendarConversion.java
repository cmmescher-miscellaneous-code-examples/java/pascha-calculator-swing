package TermProject.Fall2022.Mescher;

public class CalendarConversion {

  // Method to convert from Gregorian to Julian calendar
  // Returns an int[] with the year, month, and day
  public static int[] gregorianToJulian(int year, int month, int day) {
    int[] julianDate = new int[3];
    int jdn = gregorianToJDN(year, month, day);
    julianDate = jdnToJulian(jdn);
    return julianDate;
  }

  // Method to convert from Julian to Gregorian calendar
  // Returns an int[] with the year, month, and day
  public static int[] julianToGregorian(int year, int month, int day) {
    int[] gregorianDate = new int[3];
    int jdn = julianToJDN(year, month, day);
    gregorianDate = jdnToGregorian(jdn);
    return gregorianDate;
  }

  // Method to convert from Gregorian to Revised Julian calendar
  // Returns an int[] with the year, month, and day
  public static int[] gregorianToRevJulian(int year, int month, int day) {
    int[] revJulianDate = new int[3];
    int jdn = gregorianToJDN(year, month, day);
    revJulianDate = jdnToRevJulian(jdn);
    return revJulianDate;
  }

  // Method to convert from Revised Julian to Gregorian calendar
  // Returns an int[] with the year, month, and day
  public static int[] revJulianToGregorian(int year, int month, int day) {
    int[] gregorianDate = new int[3];
    int jdn = revJulianToJDN(year, month, day);
    gregorianDate = jdnToGregorian(jdn);
    return gregorianDate;
  }

  // Method to compute the Julian day number from Gregorian calendar date input
  // Used in order to calculate dates in other calendars
  private static int gregorianToJDN(int year, int month, int day) {
    int y = year;
    int m = month;
    int jdn; // output

    // Calculate the number of cycles of 400 years
    int cycles;
    if (y < 0) {
      cycles = y / 400 - 1;
      if (y % 400 == 0) {
        cycles++;
      }
      y = (400 - (-1 * y % 400)) % 400;
    } else {
      cycles = y / 400;
      y = y % 400;
    }

    // Treat months prior to leap day as if they were in the previous year
    if (m < 3) {
      m += 12;
      y--;
    }

    // Add contribution from number of leap years
    if (y < 0 && y % 4 != 0) {
      jdn = 365 * y + (y / 4 - 1);
    } else {
      jdn = 365 * y + (y / 4);
    }

    // Add contribution from number of months and current day in month
    jdn = jdn + 153 * (m + 1) / 5 + day - 123;

    // Adjustments for leap year differences between Julian and Gregorian calendars
    // (leap years skipped when divisible by 100 but not by 400)
    // Adjustments when year is negative (casues off by one problem with modulo for
    // leap year checks)
    if (y < 0) {
      if (y % 100 != 0) {
        if (y % 400 != 0) {
          jdn = jdn - (y / 100 - 1) + (y / 400 - 1);
        } else {
          jdn = jdn - (y / 100 - 1) + (y / 400);
        }
      } else {
        if (y % 400 != 0) {
          jdn = jdn - (y / 100) + (y / 400 - 1);
        } else {
          jdn = jdn - (y / 100) + (y / 400);
        }
      }
    } else {
      jdn = jdn - y / 100 + y / 400;
    }

    // Add base contribution for days prior to 1/1/1
    // and the total number of unaccounted cycles
    jdn = jdn + 1721120 + 146097 * cycles;

    return jdn;
  }

  // Method to compute the Julian day number from Julian calendar date input
  // Used in order to calculate dates in other calendars
  private static int julianToJDN(int year, int month, int day) {
    int y = year;
    int m = month;
    int jdn; // output

    // Calculate the number of cycles of 4years
    int cycles;
    if (y < 0) {
      cycles = y / 4 - 1;
      if (y % 4 == 0) {
        cycles++;
      }
      y = (4 - (-1 * y % 4)) % 4;
    } else {
      cycles = y / 4;
      y = y % 4;
    }

    // Treat months prior to leap day as if they were in the previous year
    if (m < 3) {
      m += 12;
      y--;
    }

    // Add contribution from number of leap years
    if (y < 0 && y % 4 != 0) {
      jdn = 365 * y + (y / 4 - 1);
    } else {
      jdn = 365 * y + (y / 4);
    }

    // Add contribution from number of months and current day in month
    jdn = jdn + 153 * (m + 1) / 5 + day - 123;

    // Add base contribution for days prior to 1/1/1
    // and the total number of unaccounted cycles
    jdn = jdn + 1721118 + 1461 * cycles;

    return jdn;
  }

  // Method to compute the Julian day number from Revised Julian calendar date
  // input
  // Used in order to calculate dates in other calendars
  private static int revJulianToJDN(int year, int month, int day) {
    int y = year;
    int m = month;
    int jdn; // output

    // Treat months prior to leap day as if they were in the previous year
    if (m < 3) {
      m += 12;
      y--;
    }

    // Add contribution from number of leap years
    if (y < 0 && y % 4 != 0) {
      jdn = 365 * y + (y / 4 - 1);
    } else {
      jdn = 365 * y + (y / 4);
    }

    // Skip leap years divisible by 100
    int jdn1 = y / 100;
    if (y < 0 && y % 100 != 0) {
      jdn1--;
    }
    // Retain leap year when 200 remainder after dividing by 900
    int jdn2i = y + 300;
    int jdn2 = jdn2i / 900;
    if (jdn2i < 0 && jdn2i % 900 != 0) {
      jdn2--;
    }
    // Retain leap year when 600 remainder after dividing by 900
    int jdn3i = y + 700;
    int jdn3 = jdn3i / 900;
    if (jdn3i < 0 && jdn3i % 900 != 0) {
      jdn3--;
    }

    // Add contribution from previous skipped and retained leap years
    jdn = jdn - jdn1 + jdn2 + jdn3;

    // Add contribution from number of months and current day in month
    int jdn4i = 153 * (m + 1);
    int jdn4 = jdn4i / 5;
    if (jdn4i < 0 && jdn4i % 5 != 0) {
      jdn4--;
    }
    jdn = jdn + jdn4 + day - 123;

    // Add base contribution for days prior to 1/1/1
    jdn = jdn + 1721120;

    return jdn;
  }

  // Method to compute Gregorian date from Julian day number
  private static int[] jdnToGregorian(int jdn) {
    int day;
    int month;
    int year;
    int[] gregorianDate = new int[3];

    // Remove contribution prior to 1/1/1
    day = jdn - 1721120;

    // Calculate the number of cycles of 400 years
    // and remove their contribution
    int cycles;
    if (day < 0) {
      cycles = day / 146097 - 1;
      if (day % 146097 == 0) {
        cycles++;
      }
      day = (146097 - (-1 * day % 146097)) % 146097;
    } else {
      cycles = day / 146097;
      day = day % 146097;
    }

    // Adjusting removal of cycles due to rounding
    int a = (4 * day + 3) / 146097;
    day = day + a - a / 4;

    // Calculating number of years contained in current day value
    // and removing their contribution from day
    year = (4 * day + 3) / 1461;
    day = day - 1461 * year / 4;

    // Calculating number of months contained in current day value
    // and removing their contribution from day
    month = (5 * day + 2) / 153;
    day = day - (153 * month + 2) / 5;
    day++;

    // Adjusting month and year values to have year start in january
    // rather than divided pre- and post-leap day
    month += 3;
    if (month > 12) {
      month -= 12;
      year++;
    }

    // Adding contribution of 400 year cycles to the year
    year = year + 400 * cycles;

    gregorianDate[0] = year;
    gregorianDate[1] = month;
    gregorianDate[2] = day;

    return gregorianDate;
  }

  // Method to compute Julian date from Julian day number
  private static int[] jdnToJulian(int jdn) {
    int day;
    int month;
    int year;
    int[] julianDate = new int[3];

    // Remove contribution prior to 1/1/1
    day = jdn - 1721118;

    // Calculate the number of cycles of 4 years
    // and remove their contribution
    int cycles;
    if (day < 0) {
      cycles = day / 1461 - 1;
      if (day % 1461 == 0) {
        cycles++;
      }
      day = (1461 - (-1 * day % 1461)) % 1461;
    } else {
      cycles = day / 1461;
      day = day % 1461;
    }

    // Calculating number of years contained in current day value
    // and removing their contribution from day
    year = (4 * day + 3) / 1461;
    day = day - 1461 * year / 4;

    // Calculating number of months contained in current day value
    // and removing their contribution from day
    month = (5 * day + 2) / 153;
    day = day - (153 * month + 2) / 5;
    day++;

    // Adjusting month and year values to have year start in january
    // rather than divided pre- and post-leap day
    month += 3;
    if (month > 12) {
      month -= 12;
      year++;
    }

    // Adding contribution of 4 year cycles to the year
    year = year + 4 * cycles;

    julianDate[0] = year;
    julianDate[1] = month;
    julianDate[2] = day;

    return julianDate;
  }

  // Method to compute Revised Julian date from Julian day number
  private static int[] jdnToRevJulian(int jdn) {
    int day;
    int month;
    int year;
    int[] revJulianDate = new int[3];

    // Remove contribution prior to 1/1/1
    day = jdn - 1721120;

    // Adjustments based on Revised Julian skipped leap years
    // Base number of leap days
    int ai = 9 * day + 2;
    int a = ai / 328718;
    if (ai < 0 && ai % 328718 != 0) {
      a--;
    }
    // Account for including years with remainder of 200 after division by 900
    int a2 = (a + 3) / 9;
    if (a < -3 && a % 9 != -3) {
      a2--;
    }
    // Account for including years with remainder of 600 after division by 900
    int a3 = (a + 7) / 9;
    if (a < -7 && a % 9 != -7) {
      a3--;
    }
    day = day + a - a2 - a3;

    // Calculating number of years contained in current day value
    // and removing their contribution from day
    int yeari = 4 * day + 3;
    year = yeari / 1461;
    if (yeari < 0 && yeari % 1461 != 0) {
      year--;
    }
    int y = 1461 * year / 4;
    if (year < 0 && 1461 * year % 4 != 0) {
      y--;
    }
    day = day - y;

    // Calculating number of months contained in current day value
    // and removing their contribution from day
    int monthi = 5 * day + 2;
    month = monthi / 153;
    if (monthi < 0 && monthi % 153 != 0) {
      month--;
    }
    int day2i = 153 * month + 2;
    int day2 = day2i / 5;
    if (day2i < 0 && day2i % 5 != 0) {
      day2--;
    }
    day = day - day2 + 1;

    // Adjusting month and year values to have year start in january
    // rather than divided pre- and post-leap day
    month += 3;
    if (month > 12) {
      month -= 12;
      year++;
    }

    revJulianDate[0] = year;
    revJulianDate[1] = month;
    revJulianDate[2] = day;

    return revJulianDate;
  }
}
