package TermProject.Fall2022.Mescher;

import java.util.Iterator;

public interface DateInput {
  Iterator<Year> iterator();
}
