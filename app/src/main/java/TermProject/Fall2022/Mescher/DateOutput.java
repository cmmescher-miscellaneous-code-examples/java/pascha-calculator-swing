package TermProject.Fall2022.Mescher;

public interface DateOutput {
  public void setDay(int day);

  public void setMonth(int month);

  public void setYear(int year);

  public int getDay();

  public int getMonth();

  public int getYear();

  public String getDateString();

  public void shift(int days);
}
