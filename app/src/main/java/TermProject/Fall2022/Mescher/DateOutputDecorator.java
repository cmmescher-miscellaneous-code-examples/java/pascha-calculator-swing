package TermProject.Fall2022.Mescher;

public interface DateOutputDecorator extends DateOutput {
  void setDay(int day);

  void setMonth(int month);

  void setYear(int year);

  int getDay();

  int getMonth();

  int getYear();

  String getDateString();

  void shift(int days);
}
