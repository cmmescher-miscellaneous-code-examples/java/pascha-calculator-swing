package TermProject.Fall2022.Mescher;

import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;

public class FileHandler {
  private String inputFilePath;
  private String outputFilePath;

  public FileHandler(String inputFilePath, String outputFilePath) {
    this.inputFilePath = inputFilePath;
    this.outputFilePath = outputFilePath;
  }

  public FileHandler(String inputFilePath) {
    this.inputFilePath = inputFilePath;
    this.outputFilePath = "output.txt";
  }

  public FileHandler() {
    this.inputFilePath = "input.txt";
    this.outputFilePath = "output.txt";
  }

  public void setInputFilePath(String inputFilePath) {
    this.inputFilePath = inputFilePath;
  }

  public void setOutputFilePath(String outputFilePath) {
    this.outputFilePath = outputFilePath;
  }

  public DateInput readInputFile() throws NumberFormatException, Exception {
    YearList inputs = new YearList();

    try (FileReader fr = new FileReader(inputFilePath); BufferedReader br = new BufferedReader(fr)) {
      String line;
      while ((line = br.readLine()) != null) {
        inputs.addYear(Year.parseYear(line));
      }
    } catch (IOException e) {
      throw new IOException("Error reading input file");
    }

    if (inputs.size() == 0) {
      throw new Exception("No valid input found. Defaulting to current year.");
    }
    return inputs;
  }

  public void writeOutputFile(ArrayList<DateOutput> outputs) throws Exception {
    try (FileWriter fw = new FileWriter(outputFilePath); BufferedWriter bw = new BufferedWriter(fw)) {
      for (DateOutput output : outputs) {
        bw.write(output.getDateString());
        bw.newLine();
      }
    } catch (Exception e) {
      throw new Exception("Error writing output file");
    }
  }
}
