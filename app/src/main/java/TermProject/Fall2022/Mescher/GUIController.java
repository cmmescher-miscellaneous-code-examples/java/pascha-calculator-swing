package TermProject.Fall2022.Mescher;

import java.util.ArrayList;
import java.util.List;

public class GUIController implements PaschaCalculatorControllerInterface {
  private GUIView view;
  private PaschaCalculatorModel model;

  public GUIController(PaschaCalculatorModel model) {
    this.model = model;
    this.view = new GUIView(model, this);
  }

  private CalculationMethod parseMethod(String method) {
    if (method.equals("julian")) {
      return new JulianCalculationMethod();
    } else if (method.equals("gregorian")) {
      return new GregorianCalculationMethod();
    } else {
      return new JulianCalculationMethod();
    }
  }

  private Class<? extends OutputCalendar> parseFormat(String format) {
    if (format.equals("julian")) {
      return JulianDateDecorator.class;
    } else if (format.equals("revisedJulian")) {
      return RevJulianDateDecorator.class;
    } else if (format.equals("gregorian")) {
      return null;
    } else {
      return JulianDateDecorator.class;
    }
  }

  private Class<? extends TargetDateDecorator> parseTarget(String target) {
    if (target.equals("pascha")) {
      return null;
    } else if (target.equals("pentecost")) {
      return PentecostCalculationDecorator.class;
    } else {
      return null;
    }
  }

  private FileHandler parseFileHandler(String inputFile, String outputFile) {
    return new FileHandler(inputFile, outputFile);
  }

  private List<Class<? extends DateOutputDecorator>> parseDecorators(List<String> decorators) {
    ArrayList<Class<? extends DateOutputDecorator>> decoratorClasses = new ArrayList<>();

    for (String decorator : decorators) {
      if (decorator.equals("byzantine")) {
        decoratorClasses.add(ByzYearDateDecorator.class);
      }
    }

    return decoratorClasses;
  }

  public void calculate(String method, String format, int year, String target, List<String> options) {
    model.reset();
    model.setCalculationMethod(parseMethod(method));
    model.setOutputCalendar(parseFormat(format));
    model.setInputDate(new Year(year));
    model.setTarget(parseTarget(target));
    for (Class<? extends DateOutputDecorator> decorator : parseDecorators(options)) {
      model.addOption(decorator);
    }
    model.calculate();
  }

  public void calculate(String method, String format, String inputFile, String outputFile, String target,
      List<String> options) {
    model.reset();
    model.setCalculationMethod(parseMethod(method));
    model.setOutputCalendar(parseFormat(format));
    model.setFileHandler(parseFileHandler(inputFile, outputFile));
    model.setTarget(parseTarget(target));
    for (Class<? extends DateOutputDecorator> decorator : parseDecorators(options)) {
      model.addOption(decorator);
    }
    model.write();
  }

  public void start() {
    view.createView();
  }

}
