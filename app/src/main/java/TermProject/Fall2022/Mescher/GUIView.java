package TermProject.Fall2022.Mescher;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class GUIView implements Observer {
  private PaschaCalculatorModelInterface model;
  private GUIController controller;
  private JLabel outputText;
  private JLabel errorText;

  public GUIView(PaschaCalculatorModelInterface model, GUIController controller) {
    this.model = model;
    this.controller = controller;
    model.addObserver(this);
  }

  public void createView() {
    JFrame frame = new JFrame("Pascha Calculator");
    frame.setLayout(new GridBagLayout());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Font headerFont = new Font("Arial", Font.BOLD, 20);
    Font subheaderFont = new Font("Arial", Font.BOLD, 16);
    Font itemFont = new Font("Arial", Font.PLAIN, 14);
    GridBagConstraints c = new GridBagConstraints();

    // Header
    JLabel headerLabel = new JLabel("Pascha Calculator");
    headerLabel.setFont(new Font("Arial", Font.BOLD, 24));

    // Input Panel

    // Input - Label
    JPanel inputPanel = new JPanel(new GridLayout(7, 1));
    JLabel inputLabel = new JLabel("Inputs: ");
    inputLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
    inputLabel.setFont(headerFont);

    inputPanel.add(inputLabel);

    // Input - Calculation Method
    JPanel calculationMethodPanel = new JPanel(new GridBagLayout());
    JLabel calculationMethodLabel = new JLabel("Calculation Method: ");
    calculationMethodLabel.setFont(subheaderFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.anchor = GridBagConstraints.PAGE_START;
    calculationMethodPanel.add(calculationMethodLabel, c);

    JRadioButton julianMethodButton = new JRadioButton("Julian");
    julianMethodButton.setFont(itemFont);
    julianMethodButton.setActionCommand("julian");
    julianMethodButton.setSelected(true);

    JRadioButton gregorianMethodButton = new JRadioButton("Gregorian");
    gregorianMethodButton.setFont(itemFont);
    gregorianMethodButton.setActionCommand("gregorian");

    ButtonGroup calculationMethodButtonGroup = new ButtonGroup();
    calculationMethodButtonGroup.add(julianMethodButton);
    calculationMethodButtonGroup.add(gregorianMethodButton);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    calculationMethodPanel.add(julianMethodButton, c);
    c.gridx = 1;
    calculationMethodPanel.add(gregorianMethodButton, c);

    inputPanel.add(calculationMethodPanel);

    // Input - Output Format
    JPanel outputFormatPanel = new JPanel(new GridBagLayout());
    JLabel outputFormatLabel = new JLabel("Output Format: ");
    outputFormatLabel.setFont(subheaderFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 4;
    c.anchor = GridBagConstraints.PAGE_START;
    outputFormatPanel.add(outputFormatLabel, c);

    JLabel outputCalendarLabel = new JLabel("Calendar: ");
    outputCalendarLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    outputFormatPanel.add(outputCalendarLabel, c);

    JRadioButton julianOutputButton = new JRadioButton("Julian");
    julianOutputButton.setFont(itemFont);
    julianOutputButton.setActionCommand("julian");
    julianOutputButton.setSelected(true);

    JRadioButton revisedJulianOutputButton = new JRadioButton("Revised Julian");
    revisedJulianOutputButton.setFont(itemFont);
    revisedJulianOutputButton.setActionCommand("revisedJulian");

    JRadioButton gregorianOutputButton = new JRadioButton("Gregorian");
    gregorianOutputButton.setFont(itemFont);
    gregorianOutputButton.setActionCommand("gregorian");

    ButtonGroup outputFormatButtonGroup = new ButtonGroup();
    outputFormatButtonGroup.add(julianOutputButton);
    outputFormatButtonGroup.add(revisedJulianOutputButton);
    outputFormatButtonGroup.add(gregorianOutputButton);
    c.gridx = 1;
    c.anchor = GridBagConstraints.LINE_START;
    c.weightx = 1.0;
    outputFormatPanel.add(julianOutputButton, c);
    c.gridx = 2;
    outputFormatPanel.add(revisedJulianOutputButton, c);
    c.gridx = 3;
    outputFormatPanel.add(gregorianOutputButton, c);
    c.weightx = 0.0;

    JLabel outputByzantineYearLabel = new JLabel("Byzantine Style Year (A.M.): ");
    outputByzantineYearLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    outputFormatPanel.add(outputByzantineYearLabel, c);

    JCheckBox outputByzantineYearCheckBox = new JCheckBox("Enabled/Disabled");
    outputByzantineYearCheckBox.setFont(itemFont);
    c.gridx = 1;
    c.anchor = GridBagConstraints.LINE_START;
    outputFormatPanel.add(outputByzantineYearCheckBox, c);

    inputPanel.add(outputFormatPanel);

    // Input - Year
    JPanel yearPanel = new JPanel(new GridBagLayout());
    JLabel yearLabel = new JLabel("Year: ");
    yearLabel.setFont(subheaderFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 3;
    c.anchor = GridBagConstraints.PAGE_START;
    yearPanel.add(yearLabel, c);

    JLabel entryMethodLabel = new JLabel("Entry Method: ");
    entryMethodLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    yearPanel.add(entryMethodLabel, c);

    JRadioButton manualEntryButton = new JRadioButton("Manual");
    manualEntryButton.setFont(itemFont);
    manualEntryButton.setActionCommand("manual");
    manualEntryButton.setSelected(true);

    JRadioButton fileEntryButton = new JRadioButton("File");
    fileEntryButton.setFont(itemFont);
    fileEntryButton.setActionCommand("file");

    ButtonGroup yearEntryMethodButtonGroup = new ButtonGroup();
    yearEntryMethodButtonGroup.add(manualEntryButton);
    yearEntryMethodButtonGroup.add(fileEntryButton);
    c.gridx = 1;
    yearPanel.add(manualEntryButton, c);
    c.gridx = 2;
    yearPanel.add(fileEntryButton, c);

    inputPanel.add(yearPanel);

    // Input - Year - Cards
    JPanel manualCard = new JPanel(new GridBagLayout());

    JLabel manualYearLabel = new JLabel("Enter Year: ");
    manualYearLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    manualCard.add(manualYearLabel, c);

    String currentYear = ((Integer) java.time.LocalDate.now().getYear()).toString();
    JTextField manualYearTextField = new JTextField(currentYear, 15);
    manualYearTextField.setFont(itemFont);
    c.gridx = 1;
    c.anchor = GridBagConstraints.LINE_START;
    manualCard.add(manualYearTextField, c);

    JPanel fileCard = new JPanel(new GridBagLayout());

    JLabel fileInputLabel = new JLabel("Input File: ");
    fileInputLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    fileCard.add(fileInputLabel, c);

    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFont(itemFont);

    JTextField fileInputTextField = new JTextField(30);
    fileInputTextField.setFont(itemFont);
    c.gridx = 1;
    c.anchor = GridBagConstraints.LINE_START;
    c.insets = new Insets(0, 0, 0, 10);
    fileCard.add(fileInputTextField, c);
    c.insets = new Insets(0, 0, 0, 0);

    JButton fileInputButton = new JButton("Browse");
    fileInputButton.setFont(itemFont);
    fileInputButton.addActionListener((ActionEvent e) -> {
      int returnVal = fileChooser.showOpenDialog(frame);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fileChooser.getSelectedFile();
        fileInputTextField.setText(file.getAbsolutePath());
      }
    });
    c.gridx = 2;
    c.anchor = GridBagConstraints.LINE_END;
    fileCard.add(fileInputButton, c);

    c.insets = new Insets(10, 0, 0, 0);
    JLabel fileOutputLabel = new JLabel("Output File: ");
    fileOutputLabel.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 1;
    c.anchor = GridBagConstraints.CENTER;
    fileCard.add(fileOutputLabel, c);

    JTextField fileOutputTextField = new JTextField(30);
    fileOutputTextField.setFont(itemFont);
    c.gridx = 1;
    c.anchor = GridBagConstraints.LINE_START;
    c.insets = new Insets(10, 0, 0, 10);
    fileCard.add(fileOutputTextField, c);
    c.insets = new Insets(10, 0, 0, 0);

    JButton fileOutputButton = new JButton("Browse");
    fileOutputButton.setFont(itemFont);
    fileOutputButton.addActionListener((ActionEvent e) -> {
      int returnVal = fileChooser.showSaveDialog(frame);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fileChooser.getSelectedFile();
        fileOutputTextField.setText(file.getAbsolutePath());
      }
    });
    c.gridx = 2;
    c.anchor = GridBagConstraints.LINE_END;
    fileCard.add(fileOutputButton, c);

    JFileChooser outputFileChooser = new JFileChooser();
    outputFileChooser.setFont(itemFont);
    c.insets = new Insets(0, 0, 0, 0);

    JPanel yearCardPanel = new JPanel(new CardLayout());
    yearCardPanel.add(manualCard, "Manual");
    yearCardPanel.add(fileCard, "File");

    manualEntryButton.addActionListener((ActionEvent e) -> {
      CardLayout cl = (CardLayout) (yearCardPanel.getLayout());
      cl.show(yearCardPanel, "Manual");
    });

    fileEntryButton.addActionListener((ActionEvent e) -> {
      CardLayout cl = (CardLayout) (yearCardPanel.getLayout());
      cl.show(yearCardPanel, "File");
    });

    inputPanel.add(yearCardPanel);

    // Input - Date to Calculate
    JPanel datePanel = new JPanel(new GridBagLayout());

    JLabel dateLabel = new JLabel("Date to Calculate: ");
    dateLabel.setFont(subheaderFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.anchor = GridBagConstraints.PAGE_START;
    datePanel.add(dateLabel, c);

    JRadioButton paschaDateButton = new JRadioButton("Pascha");
    paschaDateButton.setFont(itemFont);
    paschaDateButton.setActionCommand("pascha");
    paschaDateButton.setSelected(true);

    JRadioButton pentecostDateButton = new JRadioButton("Pentecost");
    pentecostDateButton.setFont(itemFont);
    pentecostDateButton.setActionCommand("pentecost");

    ButtonGroup dateButtonGroup = new ButtonGroup();
    dateButtonGroup.add(paschaDateButton);
    dateButtonGroup.add(pentecostDateButton);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    datePanel.add(paschaDateButton, c);
    c.gridx = 1;
    datePanel.add(pentecostDateButton, c);

    inputPanel.add(datePanel);

    // Input - Calculate Button
    JPanel calculatePanel = new JPanel(new GridBagLayout());

    JButton calculateButton = new JButton("Calculate");
    calculateButton.setFont(itemFont);
    calculateButton.addActionListener((ActionEvent e) -> {
      String method = calculationMethodButtonGroup.getSelection().getActionCommand();
      String format = outputFormatButtonGroup.getSelection().getActionCommand();
      int year;
      String inputFilePath;
      String outputFilePath;
      String target = dateButtonGroup.getSelection().getActionCommand();
      ArrayList<String> options = new ArrayList<String>();

      if (outputByzantineYearCheckBox.isSelected()) {
        options.add("byzantine");
      }

      if (yearEntryMethodButtonGroup.getSelection().getActionCommand().equals("manual")) {
        try {
          year = Integer.parseInt(manualYearTextField.getText());
          controller.calculate(method, format, year, target, options);
        } catch (NumberFormatException ex) {
          errorText.setText("Invalid Year");
          return;
        }
      } else {
        inputFilePath = fileInputTextField.getText();
        outputFilePath = fileOutputTextField.getText();
        if (inputFilePath.equals("") || outputFilePath.equals("")) {
          errorText.setText("Invalid File Path");
          return;
        }
        controller.calculate(method, format, inputFilePath, outputFilePath, target, options);
      }

    });
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.anchor = GridBagConstraints.CENTER;
    calculatePanel.add(calculateButton, c);

    inputPanel.add(calculatePanel);

    // Output Panel
    JPanel outputPanel = new JPanel(new GridLayout(2, 1));
    JLabel outputLabel = new JLabel("Outputs: ");
    outputLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
    outputLabel.setFont(headerFont);
    outputPanel.add(outputLabel);

    // Output - Output Area
    JPanel outputAreaPanel = new JPanel(new GridBagLayout());

    outputText = new JLabel("");
    outputText.setFont(itemFont);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.insets = new Insets(0, 20, 10, 0);
    c.anchor = GridBagConstraints.LINE_START;
    outputAreaPanel.add(outputText, c);

    errorText = new JLabel("");
    errorText.setFont(itemFont);
    errorText.setForeground(Color.RED);
    c.gridy = 1;
    outputAreaPanel.add(errorText, c);
    c.insets = new Insets(0, 0, 0, 0);

    outputPanel.add(outputAreaPanel);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.weightx = 0.0;
    c.weighty = 0.0;
    c.anchor = GridBagConstraints.CENTER;
    frame.add(headerLabel, c);
    c.gridy = 1;
    c.weighty = 1.0;
    c.anchor = GridBagConstraints.LINE_START;
    frame.add(inputPanel, c);
    c.gridy = 2;
    c.weighty = 0.0;
    frame.add(outputPanel, c);
    frame.setSize(600, 600);
    frame.setVisible(true);
  }

  public void updateStatus(String status) {
    outputText.setText(status);
    errorText.setText("");
  }

  public void updateError(String error) {
    errorText.setText(error);
    outputText.setText("");
  }
}
