package TermProject.Fall2022.Mescher;

import java.time.LocalDate;

public class GregorianDate implements DateOutput {
  private int year;
  private int month;
  private int day;

  public GregorianDate(int year, int month, int day) {
    this.year = year;
    this.day = day;
    this.month = month;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getDay() {
    return day;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return year;
  }

  public String getDateString() {
    return String.format("%d/%d/%d", year, month, day);
  }

  public void shift(int days) {
    LocalDate date = LocalDate.of(year, month, day);
    date = date.plusDays(days);
    year = date.getYear();
    month = date.getMonthValue();
    day = date.getDayOfMonth();
  }
}
