package TermProject.Fall2022.Mescher;

public class JulianCalculationMethod implements CalculationMethod {
  public DateOutput calculatePascha(Year inputYear) {
    int year = inputYear.getYear();
    int a = year % 4;
    int b = year % 7;
    int c = year % 19;
    int d = (19 * c + 15) % 30;
    int e = (2 * a + 4 * b - d + 34) % 7;
    int month = (d + e + 114) / 31;
    int day = (d + e + 114) % 31 + 1;
    int[] gregorianDate = CalendarConversion.julianToGregorian(year, month, day);
    return new GregorianDate(gregorianDate[0], gregorianDate[1], gregorianDate[2]);
  }
}
