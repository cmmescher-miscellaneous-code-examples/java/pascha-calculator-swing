package TermProject.Fall2022.Mescher;

public class JulianDateDecorator implements OutputCalendar {
  private GregorianDate date;
  private int year;
  private int month;
  private int day;

  public JulianDateDecorator(GregorianDate date) {
    this.date = date;
    int[] julianDate = CalendarConversion.gregorianToJulian(this.date.getYear(), this.date.getMonth(),
        this.date.getDay());
    this.year = julianDate[0];
    this.month = julianDate[1];
    this.day = julianDate[2];
  }

  public void setDay(int day) {
    this.day = day;
    int[] gregorianDate = CalendarConversion.julianToGregorian(this.year, this.month, this.day);
    date.setDay(gregorianDate[2]);
  }

  public void setMonth(int month) {
    this.month = month;
    int[] gregorianDate = CalendarConversion.julianToGregorian(this.year, this.month, this.day);
    date.setMonth(gregorianDate[1]);
  }

  public void setYear(int year) {
    this.year = year;
    int[] gregorianDate = CalendarConversion.julianToGregorian(this.year, this.month, this.day);
    date.setYear(gregorianDate[0]);
  }

  public void shift(int days) {
    date.shift(days);
    int[] julianDate = CalendarConversion.gregorianToJulian(this.date.getYear(), this.date.getMonth(),
        this.date.getDay());
    this.year = julianDate[0];
    this.month = julianDate[1];
    this.day = julianDate[2];
  }

  public int getDay() {
    return this.day;
  }

  public int getMonth() {
    return this.month;
  }

  public int getYear() {
    return this.year;
  }

  public String getDateString() {
    return String.format("%d/%d/%d", this.year, this.month, this.day);
  }
}
