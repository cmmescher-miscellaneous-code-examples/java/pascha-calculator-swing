package TermProject.Fall2022.Mescher;

public interface Observable {
  void addObserver(Observer observer);

  void removeObserver(Observer observer);

  void notifyObservers(String message);

  void notifyErrorObservers(String error);
}
