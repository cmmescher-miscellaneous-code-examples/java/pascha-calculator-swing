package TermProject.Fall2022.Mescher;

public interface Observer {
  void updateStatus(String status);

  void updateError(String error);
}
