package TermProject.Fall2022.Mescher;

import java.util.List;

public interface PaschaCalculatorControllerInterface {
  public void calculate(String method, String format, int year, String target, List<String> options);

  public void calculate(String method, String format, String inputFile, String outputFile, String target,
      List<String> options);

  public void start();

}
