package TermProject.Fall2022.Mescher;

public class PaschaCalculatorDriver {
  public static void main(String[] args) {
    PaschaCalculatorModel model = new PaschaCalculatorModel();
    GUIController controller = new GUIController(model);
    controller.start();
  }
}
