package TermProject.Fall2022.Mescher;

import java.util.ArrayList;
import java.util.Iterator;

public class PaschaCalculatorModel implements PaschaCalculatorModelInterface {
  private ArrayList<Observer> observers;
  private DateInput input;
  private CalculationMethod calculationMethod;
  private Class<? extends OutputCalendar> outputCalendar;
  private FileHandler fh;
  private ArrayList<Class<? extends DateOutputDecorator>> decorators;
  private ArrayList<DateOutput> outputs;

  // Constructors
  public PaschaCalculatorModel() {
    observers = new ArrayList<>();
    decorators = new ArrayList<Class<? extends DateOutputDecorator>>();
    outputs = new ArrayList<DateOutput>();

  }

  public void setInputDate(DateInput inputDate) {
    this.input = inputDate;
  }

  public void setCalculationMethod(CalculationMethod calculationMethod) {
    this.calculationMethod = calculationMethod;
  }

  public void setOutputCalendar(Class<? extends OutputCalendar> outputCalendar) {
    this.outputCalendar = outputCalendar;
  }

  public void setFileHandler(FileHandler fh) {
    this.fh = fh;
  }

  public void setTarget(Class<? extends TargetDateDecorator> target) {
    try {
      if (target != null) {
        this.calculationMethod = target.getConstructor(CalculationMethod.class).newInstance(this.calculationMethod);
      }
    } catch (Exception e) {
      e.printStackTrace();
      notifyErrorObservers("Invalid target date; ignoring");
    }
  }

  public void addOption(Class<? extends DateOutputDecorator> decorator) {
    decorators.add(decorator);
  }

  private void calculatePascha() {
    outputs = new ArrayList<DateOutput>();

    Iterator<Year> years = input.iterator();
    for (Year year; years.hasNext();) {
      year = years.next();
      outputs.add(calculationMethod.calculatePascha(year));
    }
  }

  private void wrapOutputCalendar(Class<? extends OutputCalendar> calendar) {
    GregorianDate output;
    if (calendar != null) {
      for (int i = 0; i < outputs.size(); i++) {
        output = (GregorianDate) outputs.get(i);
        try {
          outputs.set(i, calendar.getConstructor(GregorianDate.class).newInstance(output));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void wrap(Class<? extends DateOutputDecorator> decorator) {
    DateOutput output;
    for (int i = 0; i < outputs.size(); i++) {
      output = outputs.get(i);
      try {
        outputs.set(i, decorator.getConstructor(DateOutput.class).newInstance(output));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void calculateOutput() {
    calculatePascha();
    wrapOutputCalendar(outputCalendar);
    for (Class<? extends DateOutputDecorator> decorator : decorators) {
      wrap(decorator);
    }
  }

  public void calculate() {
    calculateOutput();
    String message = "Date: " + getOutputDateString();
    notifyObservers(message);
  }

  private void readFileInput() throws NumberFormatException, Exception {
    if (fh != null) {
      input = fh.readInputFile();
    }
  }

  private void writeFileOutput() throws Exception {
    if (fh != null) {
      fh.writeOutputFile(outputs);
    }
  }

  public void write() {
    try {
      readFileInput();
    } catch (Exception e) {
      notifyErrorObservers("Error parsing input file: " + e.getMessage());
    }
    calculateOutput();
    try {
      writeFileOutput();
    } catch (Exception e) {
      notifyErrorObservers(e.getMessage());
    }
    notifyObservers("Wrote output to file");
  }

  public void reset() {
    input = null;
    calculationMethod = null;
    outputCalendar = null;
    fh = null;
    decorators = new ArrayList<Class<? extends DateOutputDecorator>>();
    outputs = new ArrayList<DateOutput>();
  }

  public void addObserver(Observer o) {
    observers.add(o);
  }

  public void removeObserver(Observer o) {
    observers.remove(o);
  }

  public void notifyObservers(String message) {
    for (Observer o : observers) {
      o.updateStatus(message);
    }
  }

  public void notifyErrorObservers(String error) {
    for (Observer o : observers) {
      o.updateError(error);
    }
  }

  public DateOutput getOutputDate() {
    return outputs.get(outputs.size() - 1);
  }

  public String getOutputDateString() {
    return outputs.get(outputs.size() - 1).getDateString();
  }
}
