package TermProject.Fall2022.Mescher;

public interface PaschaCalculatorModelInterface extends Observable {
  void setInputDate(DateInput inputDate);

  void setCalculationMethod(CalculationMethod calculationMethod);

  void setOutputCalendar(Class<? extends OutputCalendar> outputCalendar);

  void setFileHandler(FileHandler fileHandler);

  void setTarget(Class<? extends TargetDateDecorator> target);

  void addOption(Class<? extends DateOutputDecorator> decorator);

  void calculate();

  void write();

  void reset();
}
