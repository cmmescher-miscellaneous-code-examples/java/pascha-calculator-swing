package TermProject.Fall2022.Mescher;

public class PentecostCalculationDecorator implements TargetDateDecorator {
  private CalculationMethod method;

  public PentecostCalculationDecorator(CalculationMethod date) {
    this.method = date;
  }

  public DateOutput calculatePascha(Year inputYear) {
    DateOutput pascha = method.calculatePascha(inputYear);
    pascha.shift(49);
    return pascha;
  }
}
