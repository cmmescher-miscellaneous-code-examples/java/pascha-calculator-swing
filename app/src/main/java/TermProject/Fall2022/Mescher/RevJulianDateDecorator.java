package TermProject.Fall2022.Mescher;

public class RevJulianDateDecorator implements OutputCalendar {
  private GregorianDate date;
  private int year;
  private int month;
  private int day;

  public RevJulianDateDecorator(GregorianDate date) {
    this.date = date;
    int[] revJulianDate = CalendarConversion.gregorianToRevJulian(this.date.getYear(), this.date.getMonth(),
        this.date.getDay());
    this.year = revJulianDate[0];
    this.month = revJulianDate[1];
    this.day = revJulianDate[2];
  }

  public void setDay(int day) {
    date.setDay(day);
    int[] gregorianDate = CalendarConversion.revJulianToGregorian(this.year, this.month, this.day);
    date.setDay(gregorianDate[2]);
  }

  public void setMonth(int month) {
    date.setMonth(month);
    int[] gregorianDate = CalendarConversion.revJulianToGregorian(this.year, this.month, this.day);
    date.setMonth(gregorianDate[1]);
  }

  public void setYear(int year) {
    date.setYear(year);
    int[] gregorianDate = CalendarConversion.revJulianToGregorian(this.year, this.month, this.day);
    date.setYear(gregorianDate[0]);
  }

  public void shift(int days) {
    date.shift(days);
    int[] revJulianDate = CalendarConversion.gregorianToRevJulian(this.date.getYear(), this.date.getMonth(),
        this.date.getDay());
    this.year = revJulianDate[0];
    this.month = revJulianDate[1];
    this.day = revJulianDate[2];
  }

  public int getDay() {
    return this.day;
  }

  public int getMonth() {
    return this.month;
  }

  public int getYear() {
    return this.year;
  }

  public String getDateString() {
    return String.format("%d/%d/%d", this.year, this.month, this.day);
  }
}
