package TermProject.Fall2022.Mescher;

public interface TargetDateDecorator extends CalculationMethod {
  DateOutput calculatePascha(Year inputYear);
}
