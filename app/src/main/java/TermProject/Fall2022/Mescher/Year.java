package TermProject.Fall2022.Mescher;

import java.util.Iterator;
import java.util.ArrayList;

public class Year implements DateInput {
  private int year;

  public Year(int year) {
    this.year = year;
  }

  public static Year parseYear(String yearString) {
    return new Year(Integer.parseInt(yearString));
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getYear() {
    return year;
  }

  public Iterator<Year> iterator() {
    ArrayList<Year> list = new ArrayList<Year>();
    list.add(this);
    return list.iterator();
  }

}
