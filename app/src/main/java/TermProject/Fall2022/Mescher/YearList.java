package TermProject.Fall2022.Mescher;

import java.util.Iterator;
import java.util.ArrayList;

public class YearList implements DateInput {
  // Hold a list of Years instead of DateInputs to only allow one level of nesting
  private ArrayList<Year> years = new ArrayList<Year>();

  public void addYear(Year year) {
    years.add(year);
  }

  public int size() {
    return years.size();
  }

  public Iterator<Year> iterator() {
    return years.iterator();
  }

}
